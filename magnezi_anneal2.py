import numpy as np
#import scipy.optimize as sco
import time
from numba import jit
from simanneal import Annealer
import random
from io_functions import *


class gfp(object):
    def __init__(self, dir_name='simple'):
        self.term_names, self.block_names, self.num_nodes, self.num_terms, self.block_dim = get_node_data(
            dir_name)
        self.num_blocks = self.num_nodes - self.num_terms

        self.term, self.edges, self.area = get_term_data(dir_name)

        term_block_names = self.term_names + self.block_names
        self.net_indices, self.num_nets = get_nets_data(dir_name,
                                                        term_block_names)
        self.net_indices_enum = [(i, net) for i, net in
                                 enumerate(self.net_indices)]
        half_dim = self.block_dim / 2
        self.half_dim_f = np.floor(half_dim).astype(int)
        self.half_dim_c = np.ceil(half_dim).astype(int)
        self.mid = (self.term.max(axis=1) + self.term.min(axis=1))//2
        self.partitions = np.array(partition_blocks(
            dir_name, self.net_indices, self.num_blocks, self.term, self.mid[0], self.mid[1]))
        # print(self.partitions)
        self.bounds = self.bounds_calc()

        self.min_h = self.bounds[:, :, :1].max(axis=1)
        self.max_l = self.bounds[:, :, 1:].min(axis=1)
        self.canvas_w = self.edges[1, 1] - self.edges[1, 0] + 1
        self.canvas_h = self.edges[0, 1] - self.edges[0, 0] + 1
        self.canvas = np.zeros((self.canvas_w, self.canvas_h), dtype='int')
        self.count = 0
        self.half_dist_d = []
        self.dim_add = []
        for i in range(self.num_blocks):
            self.half_dist_d.append(
                (self.block_dim[:, i:] - self.block_dim[:, :self.num_blocks-i]))
            self.dim_add.append(
                2*(self.block_dim[:, :self.num_blocks-i] + self.block_dim[:, i:]))

        #self.olr = self.overlap(self.init_guess)/self.area

    def bounds_calc(self):
        """ inclusive Min and inclusive Max co-ordinates a block can have. 
            Returns a 2 x num_blocks x 2 matrix, 
            as [ x or y, block index, min or max] """
        bound_min = self.half_dim_f + self.edges[:, :1]
        bound_max = -self.half_dim_f + self.edges[:, 1:]
        bound_min[0, self.partitions % 2 == 1] += self.mid[0]
        bound_max[0, self.partitions % 2 == 0] -= self.mid[0]
        bound_min[1, self.partitions > 1] += self.mid[1]
        bound_max[0, self.partitions <= 1] -= self.mid[1]
        return np.dstack((bound_min, bound_max))

    def wl_calc(self, block_locations):
        """        returns a 2 by (self.num_nets + 1) array where the first dimension
        corresponds to x or y, and the second dimension corresponds to the wirelength
        of the net. The last entry of dimension 2 is the total wirelength along that
        physical axis """
        wl = np.zeros((2, self.num_nets), dtype='int')
        tbl = np.hstack((self.term, block_locations))
        for i, net in self.net_indices_enum:
            wl[:, i] = np.ptp(tbl[:, net], axis=1)
        total_wl = (np.sum(wl, axis=1))
        total_wl.shape = (2, 1)
        return np.hstack((wl, total_wl))

    def overlap(self, block_loc):
        start = time.time()
        nb = self.num_blocks
        total_ol_area = 0
        for i in range(0, nb):
            if i != nb-i-1:
                diff_xy = 2*(block_loc[:, i:] - block_loc[:, :nb-i])
                ol = self.dim_add[i] + (np.absolute(self.half_dist_d[i] -
                                                    diff_xy) - np.absolute(diff_xy + self.half_dist_d[i]))
                ol[ol < 0] = 0
                ip = np.sum(ol[0, :]*ol[1, :])
                total_ol_area += ip
        return total_ol_area / (32*self.area)
        #print('it took (seconds) : ' + str(time.time() - start))


class gfp_solver(Annealer):
    def __init__(self, fp, state, granularity):
        self.fp = fp
        self.iter = 0
        self.random = np.random.randint(0, fp.num_blocks, size=(50000))
        self.rand_bits = np.random.randint(0, 2, size=(2, 50000))
        self.x_inc = round(self.fp.canvas_w / granularity)
        self.y_inc = round(self.fp.canvas_h / granularity)
        #self.start = time.time()
        super(gfp_solver, self).__init__(state)
        self.tbl = np.hstack((self.fp.term, state[:, :self.fp.num_blocks]))
#        self.update_ol(0, 0, True)

    def move(self):
        for _ in range(2):
            self._move()

    def _move(self):
        #print(time.time() - self.start)
        #self.start = time.time()
        x_or_y = self.rand_bits[0, self.iter]
        x_or_y2 = int(not x_or_y)
        block_index = self.random[self.iter]
        block_index2 = self.fp.num_blocks - block_index - 1
        inc_or_dec = self.rand_bits[1, self.iter]
        inc_or_dec2 = int(not inc_or_dec)
        self.iter += 1
        if self.iter == 50000:
            self.random = np.random.randint(0, fp.num_blocks, size=(50000))
            self.rand_bits = np.random.randint(0, 2, size=(2, 50000))
            self.iter = 0

        minb = self.fp.bounds[x_or_y, block_index, 0]
        maxb = self.fp.bounds[x_or_y, block_index, 1]
        minb2 = self.fp.bounds[x_or_y2, block_index2, 0]
        maxb2 = self.fp.bounds[x_or_y2, block_index2, 1]
        inc = self.y_inc if x_or_y else self.x_inc
        inc2 = self.y_inc if x_or_y2 else self.x_inc

        new_val = inc if inc_or_dec else -inc
        new_val2 = inc2 if inc_or_dec2 else -inc2

        new_val += self.state[x_or_y, block_index]
        new_val2 += self.state[x_or_y2, block_index2]

        if new_val < minb or new_val > maxb:
            new_val -= (2*inc if inc_or_dec else -2*inc)
        if new_val2 < minb2 or new_val2 > maxb2:
            new_val2 -= (2*inc2 if inc_or_dec2 else -2*inc2)

        self.state[x_or_y, block_index] = new_val
        self.state[x_or_y2, block_index2] = new_val2

        self.update_wl(block_index, block_index2, x_or_y, x_or_y2)
#        self.update_ol(block_index, block_index2)

    def update_ol(self, block_index, block_index2, recalc_all=False):
        start = time.time()
        for i in range(0, self.fp.num_blocks):
            index_base = self.fp.num_blocks*i - (i*(i+1))//2 + self.fp.num_blocks + self.fp.num_nets + 1
            for j in range(i+1, self.fp.num_blocks):
                index = index_base + j
                if i == block_index or j == block_index or i == block_index2 or j == block_index2 or recalc_all:
                    half_dist = ((self.fp.block_dim[:, i] - self.fp.block_dim[:, j]))
                    dim_add = (2*(self.fp.block_dim[:, i] + self.fp.block_dim[:, j]))
                    diff_xy = 2*(self.state[:, i] - self.state[:, j])
                    ol = dim_add - (abs(diff_xy - half_dist) +
                                    abs(diff_xy + half_dist))
                    ol[ol < 0] = 0
                    self.state[:, -1] += (ol - self.state[:, index])
                    self.state[:, index] = ol
                    print('it took (seconds) : ' + str(time.time() - start))

    def update_wl(self, block_index, block_index2, x_or_y, x_or_y2):
        block_index_adj = self.fp.num_terms + block_index
        block_index_adj2 = self.fp.num_terms + block_index2
        self.tbl[:, self.fp.num_terms:] = self.state[:, :self.fp.num_blocks]
        for i, net in self.fp.net_indices_enum:
            if block_index_adj in net:
                self.state[x_or_y, self.fp.num_blocks +
                           self.fp.num_nets] -= self.state[x_or_y, self.fp.num_blocks + i]
                new_wl = np.ptp(self.tbl[x_or_y, net])
                self.state[x_or_y, self.fp.num_blocks + i] = new_wl
                self.state[x_or_y, self.fp.num_blocks +
                           self.fp.num_nets] += new_wl
            if block_index_adj2 in net:
                self.state[x_or_y2, self.fp.num_blocks +
                           self.fp.num_nets] -= self.state[x_or_y2, self.fp.num_blocks + i]
                new_wl = np.ptp(self.tbl[x_or_y2, net])
                self.state[x_or_y2, self.fp.num_blocks + i] = new_wl
                self.state[x_or_y2, self.fp.num_blocks +
                           self.fp.num_nets] += new_wl

    def energy(self):
#        ol = self.state[:, -1]
        wl = self.state[:, self.fp.num_blocks + self.fp.num_nets]
#        total_ol_area = ol[0]*ol[1]
#        print(total_ol_area/(16*self.fp.area))
#        print(self.fp.overlap(self.state[:, :self.fp.num_blocks]))
        return (wl[0] + wl[1])
