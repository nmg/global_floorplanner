function [ w, h, NumNodes, NumTerminals ] = parse_node(node_str)
% this function is used to parse the *.nodes file

file_node = fopen(node_str, 'r'); 

% get NumNets
line = fgetl(file_node);
colon = find(line == ':'); 
NumNodes = str2double(line(colon+1 : end)); 

% get NumPins
line = fgetl(file_node);
colon = find(line == ':');
NumTerminals = str2double(line(colon+1 : end));

% get node dimension information
w = zeros(NumNodes-NumTerminals, 1);
h = zeros(NumNodes-NumTerminals, 1);

for i = 1 : NumTerminals
    line = fgetl(file_node);
end

for i = 1 : NumNodes - NumTerminals
    line = fgetl(file_node);
    [name, w0, h0] = parse_line(line); 
    node_id = str2double(name(2:end));
    
    w(node_id + 1) = w0;
    h(node_id + 1) = h0;
end

fclose(file_node);