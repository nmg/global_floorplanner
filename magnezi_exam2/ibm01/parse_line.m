function [name, x, y] = parse_line(line)
% this function is used to parse the line with the following format
%  string    int   int

line_r = line; 

sp_id = isspace(line); 
n1 = find(sp_id == 0, 1); 
dist = n1; 
line = line(dist+1:end);
sp_id = isspace(line);
n2 = find(sp_id == 1, 1);
line = line(n2+1:end); 
n2 = n2+dist; 
name = line_r(n1 : n2-1); 
dist = n2; 
sp_id = isspace(line); 
x1 = find(sp_id == 0, 1);
line = line(x1+1:end);
x1 = x1 + dist; 
dist = x1; 
sp_id = isspace(line);
x2 = find(sp_id == 1, 1);
line = line(x2+1:end);
x2 = x2 + dist; 
x = str2double(line_r(x1 : x2-1));
y = str2double(line);