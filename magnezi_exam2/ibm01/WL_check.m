close all 
clear
clc

% This script will help you to check the wire-length, overlap ratio and the
% area constraint of your design 
% We assume your output file is named as ibm*_output.pl. If your output file is named differently, 
% please modify the related code in this script


% define the idex of the benchmark 
i =  1; 
root_file = './'; % modify the root file to where you store the benchmark files
isPLOT = 0; % 1 : plot the placement; 0 : not plot the placement


% parse the files
tic
str = num2str(i);
if length(str) == 1
    str = ['0', str];
end

node_str = [root_file 'ibm' str '/ibm' str '.nodes'];
net_str = [root_file 'ibm' str '/ibm' str '.nets'];
out_pl_str = [root_file 'ibm' str '/ibm' str '_output.pl'];

[ width, height, NumNodes, NumTerminals ] = parse_node(node_str); 
[ Netlist ] = parse_net(net_str, NumTerminals); 
[ x, y ] = parse_place(out_pl_str);
std_cell = [x(NumTerminals+1:end) , y(NumTerminals+1:end)]; 

pin_x = x(1 : NumTerminals);
pin_y = y(1 : NumTerminals);
cell_x = x(NumTerminals+1 : end);
cell_y = y(NumTerminals+1 : end);
cell_A = sum(width .* height);

Chip_X = max(pin_x) - min(pin_x);
Chip_Y = max(pin_y) - min(pin_y);
Chip_A = Chip_X * Chip_Y; 
fprintf('Parse files: %3f [s]\n', toc);



% check the area constraint
if max(cell_x + width/2) <= max(pin_x) && max(cell_y + height/2) <= max(pin_y) && ...
        min(cell_x - width/2) >= min(pin_x) && min(cell_y - height/2) >= min(pin_y) 
    fprintf('Area Constraint Check is Passed!\n');
else
    if max(cell_x + width/2) > max(pin_x)
        fprintf('max(x_pin) is exceeded\n');
    end
    if max(cell_y + height/2) > max(pin_y)
        fprintf('max(y_pin) is exceeded\n');
    end
    if min(cell_x - width/2) < min(pin_x)
        fprintf('min(x_pin) is exceeded\n');
    end
    if min(cell_y - height/2) < min(pin_y) 
        fprintf('min(y_pin) is exceeded\n');
    end
end



% check the wire-length
tic
WL = 0; 
for i = 1 : size(Netlist, 1)
    net_nodes = Netlist(i, 2 : Netlist(i,1)+1); 
    nodes_x = x(net_nodes); 
    nodes_y = y(net_nodes);
    WL = WL + (max(nodes_x) - min(nodes_x)) + (max(nodes_y) - min(nodes_y));
end
fprintf('Calculate WL: %3f [s]\n', toc);


% check the overlap
tic
OL = 0; 
for i = 1 : size(std_cell, 1)-1
    for j = i+1 : size(std_cell, 1)

        
        
        ol_x = max(0, 0.5 * (width(i)+width(j) - abs((std_cell(i,1)-width(i)/2) - (std_cell(j,1)-width(j)/2))...
            - abs((std_cell(i,1)+width(i)/2) - (std_cell(j,1)+width(j)/2)))); 
        ol_y = max(0, 0.5 * (height(i)+height(j) - abs((std_cell(i,2)-height(i)/2) - (std_cell(j,2)-height(j)/2))...
            - abs((std_cell(i,2)+height(i)/2) - (std_cell(j,2)+height(j)/2))));
        OL = OL + ol_x * ol_y; 
    end
end      
fprintf('Cacluate OL: %3f [s]\n', toc); 

% draw the placement 
if isPLOT
    tic
    pl_fig = figure; 
    plot(x(1:NumTerminals), y(1:NumTerminals), 'r*'); 
    hold on;
    for i = 1 : length(width)
        rectangle('Position', [std_cell(i,1)-width(i)/2, std_cell(i,2)-height(i)/2, width(i), height(i)]);
    end
    fprintf('Plot placement: %3f [s]\n', toc);
end

fprintf('Total Wire-length = %d\n', WL);
fprintf('Total Overlap = %f %%\n', OL/Chip_A*100);
