import numpy as np
from subprocess import call
def get_node_data(dir_name):
    with open(dir_name + '/' + dir_name + '.nodes', 'r') as fp:
        term_names = []
        block_names = []
        xsizes = []
        ysizes = []
        for row_str in fp:
            row = row_str.split()
            if row[0] == 'NumNodes':
                num_nodes = int(row[-1])
            elif row[0] == 'NumTerminals':
                num_terms = int(row[-1])
            elif row[-1] == 'terminal':
                term_names.append(row[0])
            else:
                block_names.append(row[0])
                xsizes.append(int(row[1]))
                ysizes.append(int(row[2]))
        block_dim = np.array([xsizes, ysizes], dtype='int')
    return term_names, block_names, num_nodes, num_terms, block_dim

def get_term_data(dir_name):
    with open(dir_name + '/' + dir_name + '.pl', 'r') as fp:
        xterminals = []
        yterminals = []
        for row_str in fp:
            row = row_str.split()
            if row[0] != 'NumTerminals':
                xterminals.append(int(row[1]))
                yterminals.append(int(row[2]))
        term = np.array([xterminals, yterminals], dtype='int')
        edges = np.array([[min(xterminals), max(xterminals)],
                          [min(yterminals), max(yterminals)]], dtype='int')
        lengths = (edges[:, 1] - edges[:, 0]) + 1
        area = (lengths[0]*lengths[1])
        return term, edges, area

def get_nets_data(dir_name, term_block_names):
    with open(dir_name + '/' + dir_name + '.nets', 'r') as fp:
        lines = fp.readlines()
        i = 2
        summed_net_degrees = 0
        max_net_degree = 0
        num_nets = int(lines[0].split()[-1])
        nets = []
        while i < len(lines):
            net_degree = int(lines[i].split()[-1])
            if net_degree > max_net_degree:
                max_net_degree = net_degree
            names = [line.split()[0]
                     for line in lines[i+1: i+1 + net_degree]]
            nets.append(names)
            i += net_degree + 1
        #net_indices = np.zeros((num_nets, max_net_degree), dtype='int')
        #print(max_net_degree)
        #for i, net in enumerate(nets):
        #    net_indices[i, :len(net)] = np.array([term_block_names.index(name) for name in net],
        #             dtype='int')
        net_indices = []
        for net in nets:
            net_indices.append([term_block_names.index(name) for name in net])
        return net_indices, num_nets



def partition_blocks(dir_name, net_indices, num_blocks, terms, x_mid, y_mid):
    with open(dir_name + '/hmetis.hgr', 'w') as fp:
        fp.write(str(len(net_indices)) + ' ' + str((num_blocks + terms.shape[1])) + '\n')
        for net in net_indices:
            fp.write(' '.join(map(str, [x+1 for x in net])) + '\n')

    with open(dir_name + '/hmetis.fix', 'w') as fp:
        for i in range(terms.shape[1]):
            partition_num = 0
            if terms[0, i] >= x_mid:
                partition_num +=1
            if terms[1, i] >= y_mid:
                partition_num += (partition_num + 1)
            fp.write(str(partition_num) + '\n')
        for _ in range(num_blocks): 
            fp.write(str(-1) + '\n')
    call(['./shmetis', dir_name + '/hmetis.hgr', dir_name + '/hmetis.fix', '4', '5'])
    ret_list = []
    with open(dir_name + '/hmetis.hgr.part.4', 'r') as fp: 
        for line in fp.readlines():
            ret_list += line.split()
    return [int(x) for x in ret_list[-num_blocks:]]

def print_placement(dir_name, locations, names, num_nodes, num_term):
    with open(dir_name + '/' + dir_name + '_output.pl', 'w') as fp:
        fp.write('NumNodes : ' + str(num_nodes) + '\n')
        fp.write('NumTerminals : ' + str(num_term) + '\n')
        for i, name in enumerate(names):
            fp.write(name + ' ' + str(locations[0, i]) + ' ' +
                     str(locations[1, i]) + '\n')

