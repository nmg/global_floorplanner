function [ Netlist ] = parse_net(net_str, NumTerminals)
% this function is used to parse the *.nets file

file_net = fopen(net_str, 'r');

% get NumNets
line = fgetl(file_net);
colon = find(line == ':'); 
NumNets = str2double(line(colon+1 : end)); 

% get NumPins
line = fgetl(file_net);
colon = find(line == ':');
NumPins = str2double(line(colon+1 : end));

% get Netinfo 
Netlist = zeros(NumNets, 1000); 
cnt = 0;
while ~feof(file_net)
    line = fgetl(file_net); 
    if length(line) < 1
        break;
    end
    if line(1) == 'N'
        cnt = cnt + 1;
        colon = find(line == ':');
        degree = str2double(line(colon+1 : end));
        Netlist(cnt, 1) = degree; 
        idx = 1; 
    else
        digit_id = regexp(line, '\d');
        node_id = str2double(line(digit_id)); 
        if ~isempty(find(line == 'a', 1))
            node_id = node_id + NumTerminals + 1;
        elseif ~isempty(find(line == 'p', 1))
            node_id = node_id; 
        else
            fprintf('abnormal line\n');
        end
        idx = idx + 1; 
        Netlist(cnt, idx) = node_id; 
    end
end
        
fclose(file_net);


% trim Netlist 
max_degree = max(Netlist(:,1)); 
if max_degree + 1 < size(Netlist, 2)
    Netlist(:,max_degree+2 : end) = [];
end