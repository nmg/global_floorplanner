import numpy as np
from magnezi_anneal2 import gfp, gfp_solver
from io_functions import *
import time

start = time.time()
# Change name of benchmark here
dir_name = 'ibm01'
fp = gfp(dir_name)

# Random initialization within partitions
init_guess = (fp.bounds[:, :, 1] + fp.bounds[:, :, 0])//2
bounds_width = fp.bounds[:, :, 1] - fp.bounds[:, :, 0]
min_width = bounds_width.min(axis=1)
delta_x = np.random.randint(-min_width[0]//2,
                            min_width[0]//2, (1, fp.num_blocks))
delta_y = np.random.randint(-min_width[1]//2+1,
                            min_width[1]//2, (1, fp.num_blocks))
init_guess += np.vstack((delta_x, delta_y))

wl = fp.wl_calc(init_guess)
#ol = np.zeros((2, (fp.num_blocks+1)*fp.num_blocks//2))
state = np.hstack((init_guess, wl))
gfps = gfp_solver(fp, state, 16)
gfps.steps = 22*fp.num_blocks
gfps.updates = gfps.steps // 100
gfps.Tmax = 10**4
gfps.Tmin = 10**-2
state, wl = gfps.anneal()
print('\n')
print(state)
print('\n')
print('WL')
print(str(fp.wl_calc(state)))
print('or' + str(wl))
print('overlap')
print(fp.overlap(state[:, :fp.num_blocks]))
print('Tmax: ' + str(gfps.Tmax))
print('Tmin: ' + str(gfps.Tmin))
print('Steps: ' + str(gfps.steps))
print('State: \n' + str(state))
print('Area: ' + str(fp.canvas_w) + ' by ' + str(fp.canvas_h))
print('timing')
print(time.time() - start)
# terminal and block locations
locations = np.hstack((fp.term, state[:fp.num_blocks]))
names = fp.term_names + fp.block_names
print_placement(dir_name, locations, names, fp.num_nodes, fp.num_terms)
