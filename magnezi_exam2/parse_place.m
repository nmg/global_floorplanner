function [ x, y, NumNodes, NumTerminals ] = parse_place(pl_str)
% this function is used to parse the *.pl file

file_pl = fopen(pl_str, 'r');

% get NumNets
line = fgetl(file_pl);
colon = find(line == ':'); 
NumNodes = str2double(line(colon+1 : end)); 

% get NumPins
line = fgetl(file_pl);
colon = find(line == ':');
NumTerminals = str2double(line(colon+1 : end));

% get the placement information 
x = zeros(NumNodes, 1);
y = zeros(NumNodes, 1);

for i = 1 : NumNodes
    line = fgetl(file_pl);
    [name, x0, y0] = parse_line(line);
    node_id = str2double(name(2:end));
    if name(1) == 'a'
        node_id = node_id + NumTerminals + 1; 
    elseif name(1) == 'p'
        node_id = node_id; 
    else
        fprintf('abnormal line\n');
    end
    
    x(node_id) = x0; 
    y(node_id) = y0; 
end

fclose(file_pl);