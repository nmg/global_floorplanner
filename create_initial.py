import numpy as np
from magnezi_anneal2 import gfp, gfp_solver
import time

start = time.time()
# Change name of benchmark here
dir_name = 'ibm01'
fp = gfp(dir_name)

# Random initialization within partitions
init_guess = (fp.bounds[:, :, 1] + fp.bounds[:, :, 0])//2
delta_x = np.random.randint(-fp.canvas_w//4+1,
                            fp.canvas_w//4, (1, fp.num_blocks))
delta_y = np.random.randint(-fp.canvas_h//4+1,
                            fp.canvas_h//4, (1, fp.num_blocks))
init_guess += np.vstack((delta_x, delta_y))
# terminal and block locations
locations = np.hstack((fp.term, state[:fp.num_blocks]))
names = fp.term_names + fp.block_names
print_placement(dir_name, locations, names, fp.num_nodes, fp.num_terms)
